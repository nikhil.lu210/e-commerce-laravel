-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for panyashala_pns_db
DROP DATABASE IF EXISTS `panyashala_pns_db`;
CREATE DATABASE IF NOT EXISTS `panyashala_pns_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `panyashala_pns_db`;

-- Dumping structure for table panyashala_pns_db.banners
DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `published` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.banners: ~7 rows (approximately)
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` (`id`, `photo`, `url`, `position`, `published`, `created_at`, `updated_at`) VALUES
	(4, 'uploads/banners/banner.jpg', 'http://192.168.10.107/shop/', 1, 1, '2019-03-12 11:58:23', '2019-06-11 10:56:50'),
	(5, 'uploads/banners/banner.jpg', 'http://192.168.10.107/shop/public/', 1, 1, '2019-03-12 11:58:41', '2019-03-12 11:58:57'),
	(6, 'uploads/banners/banner.jpg', 'http://192.168.10.107/shop/public/', 2, 1, '2019-03-12 11:58:52', '2019-03-12 11:58:57'),
	(7, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-05-26 11:16:38', '2019-05-26 11:17:34'),
	(8, 'uploads/banners/banner.jpg', 'http://', 2, 1, '2019-06-11 11:00:06', '2019-06-11 11:00:27'),
	(9, 'uploads/banners/banner.jpg', 'http://', 1, 1, '2019-06-11 11:00:15', '2019-06-11 11:00:29'),
	(10, 'uploads/banners/banner.jpg', 'http://192.168.10.107/shop/', 1, 0, '2019-06-11 11:00:24', '2019-06-11 11:01:56');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.brands
DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.brands: ~2 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`id`, `name`, `logo`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
	(1, 'None', 'uploads/categories/banner/WizYw56uRmvZummVh3LdghK0oqoHBkzb3Mz69RWk.png', 0, 'none', 'None', NULL, '2019-03-12 12:05:56', '2020-01-15 07:23:48'),
	(2, 'Demo brand', 'uploads/brands/4AfNEVSvRY4R14nrMNhPpl19ImAgdP1vcy7UQhbB.jpeg', 1, 'Demo-brand-lkzCa', 'Demo brand', NULL, '2019-03-12 12:06:13', '2020-01-21 08:06:34');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.business_settings
DROP TABLE IF EXISTS `business_settings`;
CREATE TABLE IF NOT EXISTS `business_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.business_settings: ~37 rows (approximately)
/*!40000 ALTER TABLE `business_settings` DISABLE KEYS */;
INSERT INTO `business_settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'home_default_currency', '28', '2018-10-16 07:35:52', '2019-12-01 12:43:55'),
	(2, 'system_default_currency', '28', '2018-10-16 07:36:58', '2019-12-01 12:43:55'),
	(3, 'currency_format', '1', '2018-10-17 09:01:59', '2018-10-17 09:01:59'),
	(4, 'symbol_format', '1', '2018-10-17 09:01:59', '2019-01-20 08:10:55'),
	(5, 'no_of_decimals', '0', '2018-10-17 09:01:59', '2018-10-17 09:01:59'),
	(6, 'product_activation', '1', '2018-10-28 07:38:37', '2019-02-04 07:11:41'),
	(7, 'vendor_system_activation', '1', '2018-10-28 13:44:16', '2019-12-25 09:42:03'),
	(8, 'show_vendors', '1', '2018-10-28 13:44:47', '2019-02-04 07:11:13'),
	(9, 'paypal_payment', '0', '2018-10-28 13:45:16', '2019-01-31 11:09:10'),
	(10, 'stripe_payment', '0', '2018-10-28 13:45:47', '2018-11-14 07:51:51'),
	(11, 'cash_payment', '1', '2018-10-28 13:46:05', '2019-01-24 09:40:18'),
	(12, 'payumoney_payment', '0', '2018-10-28 13:46:27', '2019-03-05 11:41:36'),
	(13, 'best_selling', '1', '2018-12-24 14:13:44', '2019-02-14 11:29:13'),
	(14, 'paypal_sandbox', '1', '2019-01-16 18:44:18', '2019-01-16 18:44:18'),
	(15, 'sslcommerz_sandbox', '1', '2019-01-16 18:44:18', '2019-03-14 06:07:26'),
	(16, 'sslcommerz_payment', '0', '2019-01-24 15:39:07', '2019-01-29 12:13:46'),
	(17, 'vendor_commission', '0', '2019-01-31 12:18:04', '2019-12-01 14:19:53'),
	(18, 'verification_form', '[{"type":"text","label":"Your name"},{"type":"text","label":"Shop name"},{"type":"text","label":"Email"},{"type":"text","label":"License No"},{"type":"text","label":"Full Address"},{"type":"text","label":"Phone Number"},{"type":"file","label":"Tax Papers"}]', '2019-02-03 17:36:58', '2019-02-16 12:14:42'),
	(19, 'google_analytics', '0', '2019-02-06 18:22:35', '2019-02-06 18:22:35'),
	(20, 'facebook_login', '0', '2019-02-07 18:51:59', '2019-12-22 20:34:30'),
	(21, 'google_login', '1', '2019-02-07 18:52:10', '2019-02-09 01:41:14'),
	(22, 'twitter_login', '0', '2019-02-07 18:52:20', '2019-02-08 08:32:56'),
	(23, 'payumoney_payment', '1', '2019-03-05 17:38:17', '2019-03-05 17:38:17'),
	(24, 'payumoney_sandbox', '1', '2019-03-05 17:38:17', '2019-03-05 11:39:18'),
	(36, 'facebook_chat', '0', '2019-04-15 17:45:04', '2019-04-15 17:45:04'),
	(37, 'email_verification', '1', '2019-04-30 13:30:07', '2019-12-25 07:05:08'),
	(38, 'wallet_system', '1', '2019-05-19 14:05:44', '2019-12-22 20:34:16'),
	(39, 'coupon_system', '1', '2019-06-11 15:46:18', '2019-06-11 15:46:18'),
	(40, 'current_version', '1.5', '2019-06-11 15:46:18', '2019-06-11 15:46:18'),
	(41, 'instamojo_payment', '0', '2019-07-06 15:58:03', '2019-07-06 15:58:03'),
	(42, 'instamojo_sandbox', '1', '2019-07-06 15:58:43', '2019-07-06 15:58:43'),
	(43, 'razorpay', '0', '2019-07-06 15:58:43', '2019-12-25 09:39:04'),
	(44, 'paystack', '0', '2019-07-21 19:00:38', '2019-07-21 19:00:38'),
	(45, 'pickup_point', '1', '2019-10-17 17:50:39', '2019-12-01 14:01:06'),
	(46, 'maintenance_mode', '0', '2019-10-17 17:51:04', '2020-01-15 05:01:52'),
	(47, 'voguepay', '0', '2019-10-17 17:51:24', '2019-10-17 17:51:24'),
	(48, 'voguepay_sandbox', '0', '2019-10-17 17:51:38', '2019-10-17 17:51:38');
/*!40000 ALTER TABLE `business_settings` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` int(1) NOT NULL DEFAULT '0',
  `top` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.categories: ~6 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `banner`, `icon`, `featured`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
	(1, 'Grocery & General Stores', 'uploads/categories/banner/nnLgyCxIZXYPzKDpboPEjgTWZJJBqXPQ33TUd1I7.png', 'uploads/categories/icon/z41Eu39o0MSTUdgkpEotdvhQOzWgbSeu0ljZO3Fq.png', 1, 1, 'grocery', 'Grocery', 'Buy Grocery Items', '2019-12-24 22:31:18', '2019-12-24 17:01:18'),
	(2, 'Printing & Stationeries', 'uploads/categories/banner/WizYw56uRmvZummVh3LdghK0oqoHBkzb3Mz69RWk.png', 'uploads/categories/icon/f7U0g1BFdhmxSNxRT4MfmUOEMLU0FgmelLH77rBB.png', 1, 0, 'stationery', 'Stationery', 'Buy Stationery Items', '2019-12-24 22:32:18', '2019-12-24 17:02:18'),
	(3, 'Electronics', 'uploads/categories/banner/YWMKAx7DMESQvuP0e9c5kgVBpHswl7VBuiYO6zca.png', 'uploads/categories/icon/e4HBrcjEDyjrLECajhtKiqJDoLcZvxYNu8BX9hV0.png', 1, 1, 'Electronics', 'Electronics', 'Buy Electronics Items', '2019-12-23 05:08:42', '2019-12-22 23:38:42'),
	(4, 'Sports & Entertainment', 'uploads/categories/banner/2OxBshDm516oSkAHf0y5OaNRhyOEpaL9WBDhuVDA.png', 'uploads/categories/icon/TIpl6Y3UCAEY8ZpCxxJOZinZ2FRNwVA04SQyZzvt.png', 1, 0, 'Sports-mIYdw', 'Sports', 'Buy Sports Items', '2019-12-24 20:05:34', '2019-12-24 14:35:34'),
	(5, 'Apparel & Accessories', 'uploads/categories/banner/udFzWtKHqylWeNl2Mc74dtlYLliTgwTGw3C6XFof.png', 'uploads/categories/icon/01ezaOskprXWyfMCO558BbIAYqzvkDqdTvtfKyYy.png', 1, 0, 'Fashion-w3d4y', 'Fashion', 'Buy Clothes', '2019-12-24 20:02:51', '2019-12-24 14:32:51'),
	(6, 'Beauty & Personal care', 'uploads/categories/banner/hX6acQayXGpRSChV9wPJfWFULq03cRLrG1CXi5x6.png', 'uploads/categories/icon/XaAe8c1hDBjmQsRY5k49E1UsFXdQ0aGbOxexqUp5.png', 1, 0, 'Beauty-v7SVh', 'Buy Beauty Items', 'Buy Beauty Items', '2019-12-24 19:59:12', '2019-12-24 14:29:12');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.colors
DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.colors: ~143 rows (approximately)
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
	(1, 'IndianRed', '#CD5C5C', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(2, 'LightCoral', '#F08080', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(3, 'Salmon', '#FA8072', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(4, 'DarkSalmon', '#E9967A', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(5, 'LightSalmon', '#FFA07A', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(6, 'Crimson', '#DC143C', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(7, 'Red', '#FF0000', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(8, 'FireBrick', '#B22222', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(9, 'DarkRed', '#8B0000', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(10, 'Pink', '#FFC0CB', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(11, 'LightPink', '#FFB6C1', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(12, 'HotPink', '#FF69B4', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(13, 'DeepPink', '#FF1493', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(14, 'MediumVioletRed', '#C71585', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(15, 'PaleVioletRed', '#DB7093', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(16, 'LightSalmon', '#FFA07A', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(17, 'Coral', '#FF7F50', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(18, 'Tomato', '#FF6347', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(19, 'OrangeRed', '#FF4500', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(20, 'DarkOrange', '#FF8C00', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(21, 'Orange', '#FFA500', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(22, 'Gold', '#FFD700', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(23, 'Yellow', '#FFFF00', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(24, 'LightYellow', '#FFFFE0', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(25, 'LemonChiffon', '#FFFACD', '2018-11-05 08:12:26', '2018-11-05 08:12:26'),
	(26, 'LightGoldenrodYellow', '#FAFAD2', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(27, 'PapayaWhip', '#FFEFD5', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(28, 'Moccasin', '#FFE4B5', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(29, 'PeachPuff', '#FFDAB9', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(30, 'PaleGoldenrod', '#EEE8AA', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(31, 'Khaki', '#F0E68C', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(32, 'DarkKhaki', '#BDB76B', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(33, 'Lavender', '#E6E6FA', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(34, 'Thistle', '#D8BFD8', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(35, 'Plum', '#DDA0DD', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(36, 'Violet', '#EE82EE', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(37, 'Orchid', '#DA70D6', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(38, 'Fuchsia', '#FF00FF', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(39, 'Magenta', '#FF00FF', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(40, 'MediumOrchid', '#BA55D3', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(41, 'MediumPurple', '#9370DB', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(42, 'Amethyst', '#9966CC', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(43, 'BlueViolet', '#8A2BE2', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(44, 'DarkViolet', '#9400D3', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(45, 'DarkOrchid', '#9932CC', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(46, 'DarkMagenta', '#8B008B', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(47, 'Purple', '#800080', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(48, 'Indigo', '#4B0082', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(49, 'SlateBlue', '#6A5ACD', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(50, 'DarkSlateBlue', '#483D8B', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(51, 'MediumSlateBlue', '#7B68EE', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(52, 'GreenYellow', '#ADFF2F', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(53, 'Chartreuse', '#7FFF00', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(54, 'LawnGreen', '#7CFC00', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(55, 'Lime', '#00FF00', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(56, 'LimeGreen', '#32CD32', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(57, 'PaleGreen', '#98FB98', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(58, 'LightGreen', '#90EE90', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(59, 'MediumSpringGreen', '#00FA9A', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(60, 'SpringGreen', '#00FF7F', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(61, 'MediumSeaGreen', '#3CB371', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(62, 'SeaGreen', '#2E8B57', '2018-11-05 08:12:27', '2018-11-05 08:12:27'),
	(63, 'ForestGreen', '#228B22', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(64, 'Green', '#008000', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(65, 'DarkGreen', '#006400', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(66, 'YellowGreen', '#9ACD32', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(67, 'OliveDrab', '#6B8E23', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(68, 'Olive', '#808000', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(69, 'DarkOliveGreen', '#556B2F', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(70, 'MediumAquamarine', '#66CDAA', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(71, 'DarkSeaGreen', '#8FBC8F', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(72, 'LightSeaGreen', '#20B2AA', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(73, 'DarkCyan', '#008B8B', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(74, 'Teal', '#008080', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(75, 'Aqua', '#00FFFF', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(76, 'Cyan', '#00FFFF', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(77, 'LightCyan', '#E0FFFF', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(78, 'PaleTurquoise', '#AFEEEE', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(79, 'Aquamarine', '#7FFFD4', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(80, 'Turquoise', '#40E0D0', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(81, 'MediumTurquoise', '#48D1CC', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(82, 'DarkTurquoise', '#00CED1', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(83, 'CadetBlue', '#5F9EA0', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(84, 'SteelBlue', '#4682B4', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(85, 'LightSteelBlue', '#B0C4DE', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(86, 'PowderBlue', '#B0E0E6', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(87, 'LightBlue', '#ADD8E6', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(88, 'SkyBlue', '#87CEEB', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(89, 'LightSkyBlue', '#87CEFA', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(90, 'DeepSkyBlue', '#00BFFF', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(91, 'DodgerBlue', '#1E90FF', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(92, 'CornflowerBlue', '#6495ED', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(93, 'MediumSlateBlue', '#7B68EE', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(94, 'RoyalBlue', '#4169E1', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(95, 'Blue', '#0000FF', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(96, 'MediumBlue', '#0000CD', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(97, 'DarkBlue', '#00008B', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(98, 'Navy', '#000080', '2018-11-05 08:12:28', '2018-11-05 08:12:28'),
	(99, 'MidnightBlue', '#191970', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(100, 'Cornsilk', '#FFF8DC', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(101, 'BlanchedAlmond', '#FFEBCD', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(102, 'Bisque', '#FFE4C4', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(103, 'NavajoWhite', '#FFDEAD', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(104, 'Wheat', '#F5DEB3', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(105, 'BurlyWood', '#DEB887', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(106, 'Tan', '#D2B48C', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(107, 'RosyBrown', '#BC8F8F', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(108, 'SandyBrown', '#F4A460', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(109, 'Goldenrod', '#DAA520', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(110, 'DarkGoldenrod', '#B8860B', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(111, 'Peru', '#CD853F', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(112, 'Chocolate', '#D2691E', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(113, 'SaddleBrown', '#8B4513', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(114, 'Sienna', '#A0522D', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(115, 'Brown', '#A52A2A', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(116, 'Maroon', '#800000', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(117, 'White', '#FFFFFF', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(118, 'Snow', '#FFFAFA', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(119, 'Honeydew', '#F0FFF0', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(120, 'MintCream', '#F5FFFA', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(121, 'Azure', '#F0FFFF', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(122, 'AliceBlue', '#F0F8FF', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(123, 'GhostWhite', '#F8F8FF', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(124, 'WhiteSmoke', '#F5F5F5', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(125, 'Seashell', '#FFF5EE', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(126, 'Beige', '#F5F5DC', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(127, 'OldLace', '#FDF5E6', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(128, 'FloralWhite', '#FFFAF0', '2018-11-05 08:12:29', '2018-11-05 08:12:29'),
	(129, 'Ivory', '#FFFFF0', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(130, 'AntiqueWhite', '#FAEBD7', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(131, 'Linen', '#FAF0E6', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(132, 'LavenderBlush', '#FFF0F5', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(133, 'MistyRose', '#FFE4E1', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(134, 'Gainsboro', '#DCDCDC', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(135, 'LightGrey', '#D3D3D3', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(136, 'Silver', '#C0C0C0', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(137, 'DarkGray', '#A9A9A9', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(138, 'Gray', '#808080', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(139, 'DimGray', '#696969', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(140, 'LightSlateGray', '#778899', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(141, 'SlateGray', '#708090', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(142, 'DarkSlateGray', '#2F4F4F', '2018-11-05 08:12:30', '2018-11-05 08:12:30'),
	(143, 'Black', '#000000', '2018-11-05 08:12:30', '2018-11-05 08:12:30');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.countries: 296 rows
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `name`) VALUES
	(1, 'AF', 'Afghanistan'),
	(2, 'AL', 'Albania'),
	(3, 'DZ', 'Algeria'),
	(4, 'DS', 'American Samoa'),
	(5, 'AD', 'Andorra'),
	(6, 'AO', 'Angola'),
	(7, 'AI', 'Anguilla'),
	(8, 'AQ', 'Antarctica'),
	(9, 'AG', 'Antigua and Barbuda'),
	(10, 'AR', 'Argentina'),
	(11, 'AM', 'Armenia'),
	(12, 'AW', 'Aruba'),
	(13, 'AU', 'Australia'),
	(14, 'AT', 'Austria'),
	(15, 'AZ', 'Azerbaijan'),
	(16, 'BS', 'Bahamas'),
	(17, 'BH', 'Bahrain'),
	(18, 'BD', 'Bangladesh'),
	(19, 'BB', 'Barbados'),
	(20, 'BY', 'Belarus'),
	(21, 'BE', 'Belgium'),
	(22, 'BZ', 'Belize'),
	(23, 'BJ', 'Benin'),
	(24, 'BM', 'Bermuda'),
	(25, 'BT', 'Bhutan'),
	(26, 'BO', 'Bolivia'),
	(27, 'BA', 'Bosnia and Herzegovina'),
	(28, 'BW', 'Botswana'),
	(29, 'BV', 'Bouvet Island'),
	(30, 'BR', 'Brazil'),
	(31, 'IO', 'British Indian Ocean Territory'),
	(32, 'BN', 'Brunei Darussalam'),
	(33, 'BG', 'Bulgaria'),
	(34, 'BF', 'Burkina Faso'),
	(35, 'BI', 'Burundi'),
	(36, 'KH', 'Cambodia'),
	(37, 'CM', 'Cameroon'),
	(38, 'CA', 'Canada'),
	(39, 'CV', 'Cape Verde'),
	(40, 'KY', 'Cayman Islands'),
	(41, 'CF', 'Central African Republic'),
	(42, 'TD', 'Chad'),
	(43, 'CL', 'Chile'),
	(44, 'CN', 'China'),
	(45, 'CX', 'Christmas Island'),
	(46, 'CC', 'Cocos (Keeling) Islands'),
	(47, 'CO', 'Colombia'),
	(48, 'KM', 'Comoros'),
	(49, 'CG', 'Congo'),
	(50, 'CK', 'Cook Islands'),
	(51, 'CR', 'Costa Rica'),
	(52, 'HR', 'Croatia (Hrvatska)'),
	(53, 'CU', 'Cuba'),
	(54, 'CY', 'Cyprus'),
	(55, 'CZ', 'Czech Republic'),
	(56, 'DK', 'Denmark'),
	(57, 'DJ', 'Djibouti'),
	(58, 'DM', 'Dominica'),
	(59, 'DO', 'Dominican Republic'),
	(60, 'TP', 'East Timor'),
	(61, 'EC', 'Ecuador'),
	(62, 'EG', 'Egypt'),
	(63, 'SV', 'El Salvador'),
	(64, 'GQ', 'Equatorial Guinea'),
	(65, 'ER', 'Eritrea'),
	(66, 'EE', 'Estonia'),
	(67, 'ET', 'Ethiopia'),
	(68, 'FK', 'Falkland Islands (Malvinas)'),
	(69, 'FO', 'Faroe Islands'),
	(70, 'FJ', 'Fiji'),
	(71, 'FI', 'Finland'),
	(72, 'FR', 'France'),
	(73, 'FX', 'France, Metropolitan'),
	(74, 'GF', 'French Guiana'),
	(75, 'PF', 'French Polynesia'),
	(76, 'TF', 'French Southern Territories'),
	(77, 'GA', 'Gabon'),
	(78, 'GM', 'Gambia'),
	(79, 'GE', 'Georgia'),
	(80, 'DE', 'Germany'),
	(81, 'GH', 'Ghana'),
	(82, 'GI', 'Gibraltar'),
	(83, 'GK', 'Guernsey'),
	(84, 'GR', 'Greece'),
	(85, 'GL', 'Greenland'),
	(86, 'GD', 'Grenada'),
	(87, 'GP', 'Guadeloupe'),
	(88, 'GU', 'Guam'),
	(89, 'GT', 'Guatemala'),
	(90, 'GN', 'Guinea'),
	(91, 'GW', 'Guinea-Bissau'),
	(92, 'GY', 'Guyana'),
	(93, 'HT', 'Haiti'),
	(94, 'HM', 'Heard and Mc Donald Islands'),
	(95, 'HN', 'Honduras'),
	(96, 'HK', 'Hong Kong'),
	(97, 'HU', 'Hungary'),
	(98, 'IS', 'Iceland'),
	(99, 'IN', 'India'),
	(100, 'IM', 'Isle of Man'),
	(101, 'ID', 'Indonesia'),
	(102, 'IR', 'Iran (Islamic Republic of)'),
	(103, 'IQ', 'Iraq'),
	(104, 'IE', 'Ireland'),
	(105, 'IL', 'Israel'),
	(106, 'IT', 'Italy'),
	(107, 'CI', 'Ivory Coast'),
	(108, 'JE', 'Jersey'),
	(109, 'JM', 'Jamaica'),
	(110, 'JP', 'Japan'),
	(111, 'JO', 'Jordan'),
	(112, 'KZ', 'Kazakhstan'),
	(113, 'KE', 'Kenya'),
	(114, 'KI', 'Kiribati'),
	(115, 'KP', 'Korea, Democratic People\'s Republic of'),
	(116, 'KR', 'Korea, Republic of'),
	(117, 'XK', 'Kosovo'),
	(118, 'KW', 'Kuwait'),
	(119, 'KG', 'Kyrgyzstan'),
	(120, 'LA', 'Lao People\'s Democratic Republic'),
	(121, 'LV', 'Latvia'),
	(122, 'LB', 'Lebanon'),
	(123, 'LS', 'Lesotho'),
	(124, 'LR', 'Liberia'),
	(125, 'LY', 'Libyan Arab Jamahiriya'),
	(126, 'LI', 'Liechtenstein'),
	(127, 'LT', 'Lithuania'),
	(128, 'LU', 'Luxembourg'),
	(129, 'MO', 'Macau'),
	(130, 'MK', 'Macedonia'),
	(131, 'MG', 'Madagascar'),
	(132, 'MW', 'Malawi'),
	(133, 'MY', 'Malaysia'),
	(134, 'MV', 'Maldives'),
	(135, 'ML', 'Mali'),
	(136, 'MT', 'Malta'),
	(137, 'MH', 'Marshall Islands'),
	(138, 'MQ', 'Martinique'),
	(139, 'MR', 'Mauritania'),
	(140, 'MU', 'Mauritius'),
	(141, 'TY', 'Mayotte'),
	(142, 'MX', 'Mexico'),
	(143, 'FM', 'Micronesia, Federated States of'),
	(144, 'MD', 'Moldova, Republic of'),
	(145, 'MC', 'Monaco'),
	(146, 'MN', 'Mongolia'),
	(147, 'ME', 'Montenegro'),
	(148, 'MS', 'Montserrat'),
	(149, 'MA', 'Morocco'),
	(150, 'MZ', 'Mozambique'),
	(151, 'MM', 'Myanmar'),
	(152, 'NA', 'Namibia'),
	(153, 'NR', 'Nauru'),
	(154, 'NP', 'Nepal'),
	(155, 'NL', 'Netherlands'),
	(156, 'AN', 'Netherlands Antilles'),
	(157, 'NC', 'New Caledonia'),
	(158, 'NZ', 'New Zealand'),
	(159, 'NI', 'Nicaragua'),
	(160, 'NE', 'Niger'),
	(161, 'NG', 'Nigeria'),
	(162, 'NU', 'Niue'),
	(163, 'NF', 'Norfolk Island'),
	(164, 'MP', 'Northern Mariana Islands'),
	(165, 'NO', 'Norway'),
	(166, 'OM', 'Oman'),
	(167, 'PK', 'Pakistan'),
	(168, 'PW', 'Palau'),
	(169, 'PS', 'Palestine'),
	(170, 'PA', 'Panama'),
	(171, 'PG', 'Papua New Guinea'),
	(172, 'PY', 'Paraguay'),
	(173, 'PE', 'Peru'),
	(174, 'PH', 'Philippines'),
	(175, 'PN', 'Pitcairn'),
	(176, 'PL', 'Poland'),
	(177, 'PT', 'Portugal'),
	(178, 'PR', 'Puerto Rico'),
	(179, 'QA', 'Qatar'),
	(180, 'RE', 'Reunion'),
	(181, 'RO', 'Romania'),
	(182, 'RU', 'Russian Federation'),
	(183, 'RW', 'Rwanda'),
	(184, 'KN', 'Saint Kitts and Nevis'),
	(185, 'LC', 'Saint Lucia'),
	(186, 'VC', 'Saint Vincent and the Grenadines'),
	(187, 'WS', 'Samoa'),
	(188, 'SM', 'San Marino'),
	(189, 'ST', 'Sao Tome and Principe'),
	(190, 'SA', 'Saudi Arabia'),
	(191, 'SN', 'Senegal'),
	(192, 'RS', 'Serbia'),
	(193, 'SC', 'Seychelles'),
	(194, 'SL', 'Sierra Leone'),
	(195, 'SG', 'Singapore'),
	(196, 'SK', 'Slovakia'),
	(197, 'SI', 'Slovenia'),
	(198, 'SB', 'Solomon Islands'),
	(199, 'SO', 'Somalia'),
	(200, 'ZA', 'South Africa'),
	(201, 'GS', 'South Georgia South Sandwich Islands'),
	(202, 'SS', 'South Sudan'),
	(203, 'ES', 'Spain'),
	(204, 'LK', 'Sri Lanka'),
	(205, 'SH', 'St. Helena'),
	(206, 'PM', 'St. Pierre and Miquelon'),
	(207, 'SD', 'Sudan'),
	(208, 'SR', 'Suriname'),
	(209, 'SJ', 'Svalbard and Jan Mayen Islands'),
	(210, 'SZ', 'Swaziland'),
	(211, 'SE', 'Sweden'),
	(212, 'CH', 'Switzerland'),
	(213, 'SY', 'Syrian Arab Republic'),
	(214, 'TW', 'Taiwan'),
	(215, 'TJ', 'Tajikistan'),
	(216, 'TZ', 'Tanzania, United Republic of'),
	(217, 'TH', 'Thailand'),
	(218, 'TG', 'Togo'),
	(219, 'TK', 'Tokelau'),
	(220, 'TO', 'Tonga'),
	(221, 'TT', 'Trinidad and Tobago'),
	(222, 'TN', 'Tunisia'),
	(223, 'TR', 'Turkey'),
	(224, 'TM', 'Turkmenistan'),
	(225, 'TC', 'Turks and Caicos Islands'),
	(226, 'TV', 'Tuvalu'),
	(227, 'UG', 'Uganda'),
	(228, 'UA', 'Ukraine'),
	(229, 'AE', 'United Arab Emirates'),
	(230, 'GB', 'United Kingdom'),
	(231, 'US', 'United States'),
	(232, 'UM', 'United States minor outlying islands'),
	(233, 'UY', 'Uruguay'),
	(234, 'UZ', 'Uzbekistan'),
	(235, 'VU', 'Vanuatu'),
	(236, 'VA', 'Vatican City State'),
	(237, 'VE', 'Venezuela'),
	(238, 'VN', 'Vietnam'),
	(239, 'VG', 'Virgin Islands (British)'),
	(240, 'VI', 'Virgin Islands (U.S.)'),
	(241, 'WF', 'Wallis and Futuna Islands'),
	(242, 'EH', 'Western Sahara'),
	(243, 'YE', 'Yemen'),
	(244, 'ZR', 'Zaire'),
	(245, 'ZM', 'Zambia'),
	(246, 'ZW', 'Zimbabwe'),
	(247, 'AF', 'Afghanistan'),
	(248, 'AL', 'Albania'),
	(249, 'DZ', 'Algeria'),
	(250, 'DS', 'American Samoa'),
	(251, 'AD', 'Andorra'),
	(252, 'AO', 'Angola'),
	(253, 'AI', 'Anguilla'),
	(254, 'AQ', 'Antarctica'),
	(255, 'AG', 'Antigua and Barbuda'),
	(256, 'AR', 'Argentina'),
	(257, 'AM', 'Armenia'),
	(258, 'AW', 'Aruba'),
	(259, 'AU', 'Australia'),
	(260, 'AT', 'Austria'),
	(261, 'AZ', 'Azerbaijan'),
	(262, 'BS', 'Bahamas'),
	(263, 'BH', 'Bahrain'),
	(264, 'BD', 'Bangladesh'),
	(265, 'BB', 'Barbados'),
	(266, 'BY', 'Belarus'),
	(267, 'BE', 'Belgium'),
	(268, 'BZ', 'Belize'),
	(269, 'BJ', 'Benin'),
	(270, 'BM', 'Bermuda'),
	(271, 'BT', 'Bhutan'),
	(272, 'BO', 'Bolivia'),
	(273, 'BA', 'Bosnia and Herzegovina'),
	(274, 'BW', 'Botswana'),
	(275, 'BV', 'Bouvet Island'),
	(276, 'BR', 'Brazil'),
	(277, 'IO', 'British Indian Ocean Territory'),
	(278, 'BN', 'Brunei Darussalam'),
	(279, 'BG', 'Bulgaria'),
	(280, 'BF', 'Burkina Faso'),
	(281, 'BI', 'Burundi'),
	(282, 'KH', 'Cambodia'),
	(283, 'CM', 'Cameroon'),
	(284, 'CA', 'Canada'),
	(285, 'CV', 'Cape Verde'),
	(286, 'KY', 'Cayman Islands'),
	(287, 'CF', 'Central African Republic'),
	(288, 'TD', 'Chad'),
	(289, 'CL', 'Chile'),
	(290, 'CN', 'China'),
	(291, 'CX', 'Christmas Island'),
	(292, 'CC', 'Cocos (Keeling) Islands'),
	(293, 'CO', 'Colombia'),
	(294, 'KM', 'Comoros'),
	(295, 'CG', 'Congo'),
	(296, 'CK', 'Cook Islands');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.coupons
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL,
  `discount_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(15) NOT NULL,
  `end_date` int(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.coupons: ~0 rows (approximately)
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.coupon_usages
DROP TABLE IF EXISTS `coupon_usages`;
CREATE TABLE IF NOT EXISTS `coupon_usages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.coupon_usages: ~0 rows (approximately)
/*!40000 ALTER TABLE `coupon_usages` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_usages` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.currencies
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` double(10,5) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.currencies: ~25 rows (approximately)
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` (`id`, `name`, `symbol`, `exchange_rate`, `status`, `code`, `created_at`, `updated_at`) VALUES
	(1, 'U.S. Dollar', '$', 1.00000, 1, 'USD', '2018-10-09 17:35:08', '2018-10-17 11:50:52'),
	(2, 'Australian Dollar', '$', 1.28000, 1, 'AUD', '2018-10-09 17:35:08', '2019-02-04 11:51:55'),
	(5, 'Brazilian Real', 'R$', 3.25000, 1, 'BRL', '2018-10-09 17:35:08', '2018-10-17 11:51:00'),
	(6, 'Canadian Dollar', '$', 1.27000, 1, 'CAD', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(7, 'Czech Koruna', 'Kč', 20.65000, 1, 'CZK', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(8, 'Danish Krone', 'kr', 6.05000, 1, 'DKK', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(9, 'Euro', '€', 0.85000, 1, 'EUR', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(10, 'Hong Kong Dollar', '$', 7.83000, 1, 'HKD', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(11, 'Hungarian Forint', 'Ft', 255.24000, 1, 'HUF', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(12, 'Israeli New Sheqel', '₪', 3.48000, 1, 'ILS', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(13, 'Japanese Yen', '¥', 107.12000, 1, 'JPY', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(14, 'Malaysian Ringgit', 'RM', 3.91000, 1, 'MYR', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(15, 'Mexican Peso', '$', 18.72000, 1, 'MXN', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(16, 'Norwegian Krone', 'kr', 7.83000, 1, 'NOK', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(17, 'New Zealand Dollar', '$', 1.38000, 1, 'NZD', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(18, 'Philippine Peso', '₱', 52.26000, 1, 'PHP', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(19, 'Polish Zloty', 'zł', 3.39000, 1, 'PLN', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(20, 'Pound Sterling', '£', 0.72000, 1, 'GBP', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(21, 'Russian Ruble', 'руб', 55.93000, 1, 'RUB', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(22, 'Singapore Dollar', '$', 1.32000, 1, 'SGD', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(23, 'Swedish Krona', 'kr', 8.19000, 1, 'SEK', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(24, 'Swiss Franc', 'CHF', 0.94000, 1, 'CHF', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(26, 'Thai Baht', '฿', 31.39000, 1, 'THB', '2018-10-09 17:35:08', '2018-10-09 17:35:08'),
	(27, 'Taka', '৳', 84.00000, 1, 'BDT', '2018-10-09 17:35:08', '2018-12-02 11:16:13'),
	(28, 'Indian Rupee', 'Rs', 68.45000, 1, 'Rupee', '2019-07-07 16:33:46', '2019-07-07 16:33:46');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.customers
DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_customer_user_id` (`user_id`),
  CONSTRAINT `fk_customer_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.customers: ~6 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
	(12, 20, '2019-12-22 05:26:28', '2019-12-22 05:26:28'),
	(14, 31, '2019-12-24 20:48:24', '2019-12-24 20:48:24'),
	(15, 32, '2019-12-24 20:51:11', '2019-12-24 20:51:11'),
	(16, 33, '2019-12-24 22:37:46', '2019-12-24 22:37:46'),
	(17, 34, '2020-01-18 08:37:14', '2020-01-18 08:37:14');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.flash_deals
DROP TABLE IF EXISTS `flash_deals`;
CREATE TABLE IF NOT EXISTS `flash_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` int(20) DEFAULT NULL,
  `end_date` int(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.flash_deals: ~0 rows (approximately)
/*!40000 ALTER TABLE `flash_deals` DISABLE KEYS */;
INSERT INTO `flash_deals` (`id`, `title`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`) VALUES
	(4, '90% Off', 1579046400, 1580601600, 1, '2020-01-15 07:31:24', '2020-01-15 08:10:55');
/*!40000 ALTER TABLE `flash_deals` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.flash_deal_products
DROP TABLE IF EXISTS `flash_deal_products`;
CREATE TABLE IF NOT EXISTS `flash_deal_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flash_deal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount` double(8,2) DEFAULT '0.00',
  `discount_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.flash_deal_products: ~4 rows (approximately)
/*!40000 ALTER TABLE `flash_deal_products` DISABLE KEYS */;
INSERT INTO `flash_deal_products` (`id`, `flash_deal_id`, `product_id`, `discount`, `discount_type`, `created_at`, `updated_at`) VALUES
	(25, 4, 14, 90.00, 'percent', '2020-01-15 08:10:55', '2020-01-15 08:10:55'),
	(26, 4, 17, 90.00, 'percent', '2020-01-15 08:10:55', '2020-01-15 08:10:55'),
	(27, 4, 28, 90.00, 'percent', '2020-01-15 08:10:55', '2020-01-15 08:10:55'),
	(28, 4, 29, 90.00, 'percent', '2020-01-15 08:10:55', '2020-01-15 08:10:55');
/*!40000 ALTER TABLE `flash_deal_products` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.general_settings
DROP TABLE IF EXISTS `general_settings`;
CREATE TABLE IF NOT EXISTS `general_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `frontend_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_sidebar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.general_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_settings` DISABLE KEYS */;
INSERT INTO `general_settings` (`id`, `frontend_color`, `logo`, `admin_logo`, `admin_login_background`, `admin_login_sidebar`, `favicon`, `site_name`, `address`, `description`, `phone`, `email`, `facebook`, `instagram`, `twitter`, `youtube`, `google_plus`, `created_at`, `updated_at`) VALUES
	(1, 'default', 'uploads/logo/logo2.png', 'uploads/admin_logo/wCgHrz0Q5QoL1yu4vdrNnQIr4uGuNL48CXfcxOuS.png', NULL, NULL, 'uploads/favicon/uHdGidSaRVzvPgDj6JFtntMqzJkwDk9659233jrb.png', 'Panyashala', 'India', 'Online Marketplace', '7788998877', 'info@panyashala.com', 'https://www.facebook.com', 'https://www.instagram.com', 'https://www.twitter.com', 'https://www.youtube.com', 'https://www.googleplus.com', '2019-12-24 18:10:51', '2019-12-24 12:40:51');
/*!40000 ALTER TABLE `general_settings` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.home_categories
DROP TABLE IF EXISTS `home_categories`;
CREATE TABLE IF NOT EXISTS `home_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `subsubcategories` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.home_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `home_categories` DISABLE KEYS */;
INSERT INTO `home_categories` (`id`, `category_id`, `subsubcategories`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, '["1"]', 1, '2019-03-12 12:38:23', '2019-03-12 12:38:23'),
	(2, 2, '["10"]', 1, '2019-03-12 12:44:54', '2019-03-12 12:44:54');
/*!40000 ALTER TABLE `home_categories` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.languages
DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rtl` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.languages: ~3 rows (approximately)
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`id`, `name`, `code`, `rtl`, `created_at`, `updated_at`) VALUES
	(1, 'English', 'en', 0, '2019-01-20 18:13:20', '2019-01-20 18:13:20'),
	(4, 'Arabic', 'sa', 1, '2019-04-29 00:34:12', '2019-04-29 00:34:12'),
	(5, 'Hindi', 'in', 0, '2019-12-25 09:24:47', '2019-12-25 09:24:47');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.links
DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.links: ~2 rows (approximately)
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` (`id`, `name`, `url`, `created_at`, `updated_at`) VALUES
	(1, 'Terms', '/terms', '2020-01-15 12:53:15', '2020-01-15 07:23:15'),
	(2, 'Privacy Policy', '/privacypolicy', '2020-01-15 12:53:26', '2020-01-15 07:23:26');
/*!40000 ALTER TABLE `links` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.migrations: ~3 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_12_14_133857_create_sessions_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.new
DROP TABLE IF EXISTS `new`;
CREATE TABLE IF NOT EXISTS `new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table panyashala_pns_db.new: 0 rows
/*!40000 ALTER TABLE `new` DISABLE KEYS */;
/*!40000 ALTER TABLE `new` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `shipping_address` longtext COLLATE utf8_unicode_ci,
  `payment_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'unpaid',
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `grand_total` double(8,2) DEFAULT NULL,
  `coupon_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `code` mediumtext COLLATE utf8_unicode_ci,
  `date` int(20) NOT NULL,
  `viewed` int(1) NOT NULL DEFAULT '0',
  `delivery_viewed` int(1) NOT NULL DEFAULT '0',
  `payment_status_viewed` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.orders: ~2 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user_id`, `guest_id`, `shipping_address`, `payment_type`, `payment_status`, `payment_details`, `grand_total`, `coupon_discount`, `code`, `date`, `viewed`, `delivery_viewed`, `payment_status_viewed`, `created_at`, `updated_at`) VALUES
	(30, 20, NULL, '{"name":"Satyanand Singh","email":"satyanandsingh457@gmail.com","address":"IGIMS, Sheikhpura, Patna, Bihar 800014, India","country":"India","city":"Patna","postal_code":"800014","phone":"7320991127","checkout_type":"logged"}', 'cash_on_delivery', 'unpaid', NULL, 100.00, 0.00, '20191230-111357', 1577704437, 1, 0, 0, '2019-12-30 11:43:57', '2020-01-15 08:18:02'),
	(32, NULL, 970472, '{"name":"Test","email":"abc@mail.com","address":"RD No 9","country":"United States","city":"Cal","postal_code":"51234","phone":"8888888888","checkout_type":"guest"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200101-034640', 1577893600, 0, 0, 0, '2020-01-01 16:16:40', '2020-01-01 16:16:40');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.order_details
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `variation` longtext COLLATE utf8_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `tax` double(8,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` double(8,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) DEFAULT NULL,
  `payment_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid',
  `delivery_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'pending',
  `shipping_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_point_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` (`id`, `order_id`, `seller_id`, `product_id`, `variation`, `price`, `tax`, `shipping_cost`, `quantity`, `payment_status`, `delivery_status`, `shipping_type`, `pickup_point_id`, `created_at`, `updated_at`) VALUES
	(71, 30, 16, 17, NULL, 100.00, 0.00, 0.00, 2, 'unpaid', 'pending', 'home_delivery', NULL, '2019-12-30 11:43:57', '2019-12-30 11:43:57');
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.payments
DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.payments: ~0 rows (approximately)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`id`, `seller_id`, `amount`, `payment_details`, `payment_method`, `created_at`, `updated_at`) VALUES
	(1, 1, 20.00, '{"status":true,"message":"Verification successful","data":{"id":221606204,"domain":"test","status":"success","reference":"Ikp0DAykc9zOmiv1A5uoyXHkK","amount":2000,"message":null,"gateway_response":"Successful","paid_at":"2019-07-23T12:44:59.000Z","created_at":"2019-07-23T12:44:52.000Z","channel":"card","currency":"NGN","ip_address":"103.106.237.179","metadata":"","log":{"start_time":1563885895,"time_spent":5,"attempts":1,"errors":0,"success":true,"mobile":false,"input":[],"history":[{"type":"action","message":"Attempted to pay with card","time":4},{"type":"success","message":"Successfully paid with card","time":5}]},"fees":30,"fees_split":null,"authorization":{"authorization_code":"AUTH_3x32ugus56","bin":"408408","last4":"4081","exp_month":"12","exp_year":"2020","channel":"card","card_type":"visa DEBIT","bank":"Test Bank","country_code":"NG","brand":"visa","reusable":true,"signature":"SIG_7IqA0VMXXbqi1aV2jKf7"},"customer":{"id":10146940,"first_name":null,"last_name":null,"email":"admin@example.com","customer_code":"CUS_z28gvc4unmu8rmn","phone":null,"metadata":null,"risk_action":"default"},"plan":null,"order_id":null,"paidAt":"2019-07-23T12:44:59.000Z","createdAt":"2019-07-23T12:44:52.000Z","transaction_date":"2019-07-23T12:44:52.000Z","plan_object":[],"subaccount":[]}}', 'paystack', '2019-07-23 12:45:03', '2019-07-23 12:45:03');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.pickup_points
DROP TABLE IF EXISTS `pickup_points`;
CREATE TABLE IF NOT EXISTS `pickup_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `pick_up_status` int(1) DEFAULT NULL,
  `cash_on_pickup_status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table panyashala_pns_db.pickup_points: ~0 rows (approximately)
/*!40000 ALTER TABLE `pickup_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `pickup_points` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.policies
DROP TABLE IF EXISTS `policies`;
CREATE TABLE IF NOT EXISTS `policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.policies: ~5 rows (approximately)
/*!40000 ALTER TABLE `policies` DISABLE KEYS */;
INSERT INTO `policies` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
	(1, 'support_policy', NULL, '2019-10-29 18:54:45', '2019-01-22 11:13:15'),
	(2, 'return_policy', NULL, '2019-10-29 18:54:47', '2019-01-24 11:40:11'),
	(4, 'seller_policy', NULL, '2019-10-29 18:54:49', '2019-02-04 23:50:15'),
	(5, 'terms', NULL, '2019-10-29 18:54:51', '2019-10-29 00:00:00'),
	(6, 'privacy_policy', NULL, '2019-10-29 18:54:54', '2019-10-29 00:00:00');
/*!40000 ALTER TABLE `policies` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `added_by` varchar(6) NOT NULL DEFAULT 'admin',
  `user_id` int(10) unsigned NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `subsubcategory_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `photos` varchar(2000) DEFAULT NULL,
  `thumbnail_img` varchar(100) DEFAULT NULL,
  `featured_img` varchar(100) DEFAULT NULL,
  `flash_deal_img` varchar(100) DEFAULT NULL,
  `video_provider` varchar(20) DEFAULT NULL,
  `video_link` varchar(100) DEFAULT NULL,
  `tags` mediumtext,
  `description` longtext,
  `unit_price` double(8,2) NOT NULL,
  `purchase_price` double(8,2) NOT NULL,
  `choice_options` mediumtext,
  `colors` mediumtext,
  `variations` mediumtext NOT NULL,
  `todays_deal` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '1',
  `featured` int(11) NOT NULL DEFAULT '0',
  `online` int(11) NOT NULL DEFAULT '0',
  `current_stock` int(10) NOT NULL DEFAULT '0',
  `unit` varchar(20) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `discount_type` varchar(10) DEFAULT NULL,
  `tax` double(8,2) DEFAULT NULL,
  `tax_type` varchar(10) DEFAULT NULL,
  `shipping_type` varchar(20) NOT NULL DEFAULT 'flat_rate',
  `shipping_cost` double(8,2) DEFAULT '0.00',
  `num_of_sale` int(11) NOT NULL DEFAULT '0',
  `meta_title` mediumtext,
  `meta_description` longtext,
  `meta_img` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `slug` mediumtext NOT NULL,
  `rating` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `subcategory_id` (`subcategory_id`),
  KEY `subsubcategory_id` (`subsubcategory_id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_ibfk_2` FOREIGN KEY (`subcategory_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_ibfk_3` FOREIGN KEY (`subsubcategory_id`) REFERENCES `sub_sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_ibfk_4` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table panyashala_pns_db.products: ~5 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `online`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `rating`, `created_at`, `updated_at`) VALUES
	(14, 'Disappearing Earth - کتاب', 'seller', 13, 2, 5, 18, 1, '["uploads\\/products\\/photos\\/hIAFabQEVhfLUjzqzuKMGmzAGaWZkvXM1qtF6aqW.png"]', 'uploads/products/thumbnail/1M2FRk1O7E9VZfaL5UofgkLCaTzI6TFu2vvBR7Ov.png', 'uploads/products/thumbnail/1M2FRk1O7E9VZfaL5UofgkLCaTzI6TFu2vvBR7Ov.png', NULL, 'youtube', NULL, '', NULL, 500.00, 450.00, '[]', '[]', '[]', 1, 1, 1, 0, 289, 'Pc', 0.00, 'amount', 0.00, 'amount', 'free', 0.00, 23, NULL, NULL, NULL, NULL, 'Book----r2frm', 0.00, '2019-12-14 03:46:39', '2019-12-24 20:19:27'),
	(17, 'Liddells Milk', 'seller', 16, 1, 1, 1, 1, '["uploads\\/products\\/photos\\/AJhrV32JaytmOMTZdho7pokRL5YPMEebCGYkyUpV.png"]', 'uploads/products/thumbnail/V17jfMtInG8vCOfbRp48AIq1kBWhgnxsPE1jNZlL.png', 'uploads/products/thumbnail/V17jfMtInG8vCOfbRp48AIq1kBWhgnxsPE1jNZlL.png', NULL, 'youtube', NULL, 'test', NULL, 50.00, 45.00, '[]', '[]', '[]', 1, 1, 1, 1, 42, 'Kg', 0.00, 'amount', 0.00, 'amount', 'free', 0.00, 2, NULL, NULL, 'uploads/products/meta/aB3Al8Xd846eT5icy5Kz0TxW5XFYs7TqUAc2dnLF.jpeg', NULL, 'Liddells-Milk-3mjhL', 0.00, '2019-12-11 05:53:55', '2020-01-21 06:28:18'),
	(28, 'बासमती चावल, Rice', 'seller', 16, 1, 2, 5, 1, '["uploads\\/products\\/photos\\/xF8K8drwoYTVrOPnRaMQZgZKuhEEcPrrE1nvcT0H.png"]', 'uploads/products/thumbnail/L3SWrKkybL7DRzUWF5h0CHlDWCa0lWgTGDJTcQo1.png', 'uploads/products/thumbnail/L3SWrKkybL7DRzUWF5h0CHlDWCa0lWgTGDJTcQo1.png', NULL, 'youtube', NULL, 'test', NULL, 50.00, 45.00, '[]', '[]', '[]', 1, 1, 1, 0, 30, 'Kg', 0.00, 'amount', 0.00, 'amount', 'free', 0.00, 42, NULL, NULL, 'uploads/products/meta/aB3Al8Xd846eT5icy5Kz0TxW5XFYs7TqUAc2dnLF.jpeg', NULL, '--Rice-MlIKV', 0.00, '2019-12-23 00:08:46', '2019-12-25 07:53:05'),
	(29, 'बासमती चावल, Rice', 'seller', 16, 1, 2, 5, 1, '["uploads\\/products\\/photos\\/xF8K8drwoYTVrOPnRaMQZgZKuhEEcPrrE1nvcT0H.png"]', 'uploads/products/thumbnail/L3SWrKkybL7DRzUWF5h0CHlDWCa0lWgTGDJTcQo1.png', 'uploads/products/thumbnail/L3SWrKkybL7DRzUWF5h0CHlDWCa0lWgTGDJTcQo1.png', NULL, 'youtube', NULL, 'test', NULL, 50.00, 45.00, '[]', '[]', '[]', 1, 1, 1, 0, 30, 'Kg', 0.00, 'amount', 0.00, 'amount', 'free', 0.00, 42, NULL, NULL, 'uploads/products/meta/aB3Al8Xd846eT5icy5Kz0TxW5XFYs7TqUAc2dnLF.jpeg', NULL, '--Rice-AQRQ7', 0.00, '2019-12-25 07:56:13', '2020-01-21 08:59:52'),
	(30, 'Liddells Milk', 'seller', 16, 1, 1, 1, 1, '["uploads\\/products\\/photos\\/AJhrV32JaytmOMTZdho7pokRL5YPMEebCGYkyUpV.png"]', 'uploads/products/thumbnail/V17jfMtInG8vCOfbRp48AIq1kBWhgnxsPE1jNZlL.png', 'uploads/products/thumbnail/V17jfMtInG8vCOfbRp48AIq1kBWhgnxsPE1jNZlL.png', NULL, 'youtube', NULL, 'test', NULL, 50.00, 45.00, '[]', '[]', '[]', 1, 1, 1, 1, 42, 'Kg', 0.00, 'amount', 0.00, 'amount', 'free', 0.00, 2, NULL, NULL, 'uploads/products/meta/aB3Al8Xd846eT5icy5Kz0TxW5XFYs7TqUAc2dnLF.jpeg', NULL, 'Liddells-Milk-Vih1m', 0.00, '2020-01-21 07:03:51', '2020-01-21 07:03:51');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.product_stocks
DROP TABLE IF EXISTS `product_stocks`;
CREATE TABLE IF NOT EXISTS `product_stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `stocks` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.product_stocks: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_stocks` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.providers
DROP TABLE IF EXISTS `providers`;
CREATE TABLE IF NOT EXISTS `providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `verification_status` int(1) NOT NULL DEFAULT '0',
  `verification_info` longtext COLLATE utf8_unicode_ci,
  `cash_on_delivery_status` int(1) NOT NULL DEFAULT '0',
  `sslcommerz_status` int(1) NOT NULL DEFAULT '0',
  `stripe_status` int(1) DEFAULT '0',
  `paypal_status` int(1) NOT NULL DEFAULT '0',
  `paypal_client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_client_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssl_store_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssl_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instamojo_status` int(1) NOT NULL DEFAULT '0',
  `instamojo_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razorpay_status` int(1) NOT NULL DEFAULT '0',
  `razorpay_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razorpay_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paystack_status` int(1) NOT NULL DEFAULT '0',
  `paystack_public_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paystack_secret_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voguepay_status` int(1) NOT NULL DEFAULT '0',
  `voguepay_merchand_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_to_pay` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_provider_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.providers: ~1 rows (approximately)
/*!40000 ALTER TABLE `providers` DISABLE KEYS */;
INSERT INTO `providers` (`id`, `user_id`, `verification_status`, `verification_info`, `cash_on_delivery_status`, `sslcommerz_status`, `stripe_status`, `paypal_status`, `paypal_client_id`, `paypal_client_secret`, `ssl_store_id`, `ssl_password`, `stripe_key`, `stripe_secret`, `instamojo_status`, `instamojo_api_key`, `instamojo_token`, `razorpay_status`, `razorpay_api_key`, `razorpay_secret`, `paystack_status`, `paystack_public_key`, `paystack_secret_key`, `voguepay_status`, `voguepay_merchand_id`, `admin_to_pay`, `created_at`, `updated_at`) VALUES
	(9, 8, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2020-01-21 17:16:35', '2020-01-21 17:16:35');
/*!40000 ALTER TABLE `providers` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.reviews
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.reviews: ~0 rows (approximately)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
	(1, 'Manager', '["1","2","4"]', '2018-10-10 10:39:47', '2018-10-10 10:51:37'),
	(2, 'Accountant', '["2","3"]', '2018-10-10 10:52:09', '2018-10-10 10:52:09');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.searches
DROP TABLE IF EXISTS `searches`;
CREATE TABLE IF NOT EXISTS `searches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.searches: ~28 rows (approximately)
/*!40000 ALTER TABLE `searches` DISABLE KEYS */;
INSERT INTO `searches` (`id`, `query`, `count`, `created_at`, `updated_at`) VALUES
	(44, 'Food', 1, '2019-12-22 03:38:51', '2019-12-22 03:38:51'),
	(47, 'Pen', 2, '2019-12-22 17:59:15', '2019-12-22 17:59:15'),
	(49, 'Book', 2, '2019-12-22 21:24:28', '2019-12-23 06:00:39'),
	(50, 'दूध', 1, '2019-12-22 23:21:34', '2019-12-22 23:21:34'),
	(53, 'Basmati', 3, '2019-12-23 01:01:03', '2019-12-30 11:39:19'),
	(54, 'बासमती', 1, '2019-12-23 01:02:18', '2019-12-23 01:02:18'),
	(55, 'T', 2, '2019-12-23 01:23:04', '2019-12-23 01:23:04'),
	(56, 'laptop', 1, '2019-12-23 06:10:03', '2019-12-23 06:10:03'),
	(57, 'b', 1, '2019-12-24 09:47:38', '2019-12-24 09:47:38'),
	(58, 'p', 1, '2019-12-24 09:47:45', '2019-12-24 09:47:45'),
	(59, 'egg', 1, '2019-12-24 10:12:31', '2019-12-24 10:12:31'),
	(60, 'milk', 10, '2019-12-24 10:12:43', '2020-01-15 06:33:38'),
	(61, 'restaurant', 9, '2019-12-24 10:17:56', '2020-01-19 05:24:07'),
	(62, 'Mehta', 1, '2019-12-24 12:08:17', '2019-12-24 12:08:17'),
	(63, 'Mehta market', 2, '2019-12-24 12:08:29', '2019-12-25 07:10:04'),
	(64, 'general stores', 1, '2019-12-24 18:10:46', '2019-12-24 18:10:46'),
	(65, 'F', 1, '2019-12-25 06:25:17', '2019-12-25 06:25:17'),
	(66, 'बासमती चावल', 2, '2019-12-25 07:57:03', '2019-12-25 07:59:50'),
	(67, 'Electrician', 1, '2019-12-25 08:48:09', '2019-12-25 08:48:09'),
	(68, 'AC repairt', 1, '2019-12-25 08:48:22', '2019-12-25 08:48:22'),
	(69, 'ब', 2, '2019-12-26 03:33:37', '2019-12-26 03:33:53'),
	(70, 'Fruits', 2, '2019-12-30 13:53:57', '2019-12-30 13:53:57'),
	(71, 'n', 2, '2020-01-15 06:08:23', '2020-01-15 06:10:49'),
	(72, 'h', 2, '2020-01-15 06:12:20', '2020-01-15 06:13:11'),
	(73, 'te', 1, '2020-01-15 06:13:37', '2020-01-15 06:13:37'),
	(74, 'df', 1, '2020-01-18 08:52:31', '2020-01-18 08:52:31'),
	(75, 'k', 1, '2020-01-18 08:56:50', '2020-01-18 08:56:50'),
	(76, 'hskjda', 3, '2020-01-18 09:47:26', '2020-01-18 09:48:40');
/*!40000 ALTER TABLE `searches` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.sellers
DROP TABLE IF EXISTS `sellers`;
CREATE TABLE IF NOT EXISTS `sellers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `verification_status` int(1) NOT NULL DEFAULT '0',
  `verification_info` longtext COLLATE utf8_unicode_ci,
  `cash_on_delivery_status` int(1) NOT NULL DEFAULT '0',
  `sslcommerz_status` int(1) NOT NULL DEFAULT '0',
  `stripe_status` int(1) DEFAULT '0',
  `paypal_status` int(1) NOT NULL DEFAULT '0',
  `paypal_client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_client_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssl_store_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssl_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instamojo_status` int(1) NOT NULL DEFAULT '0',
  `instamojo_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razorpay_status` int(1) NOT NULL DEFAULT '0',
  `razorpay_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razorpay_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paystack_status` int(1) NOT NULL DEFAULT '0',
  `paystack_public_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paystack_secret_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voguepay_status` int(1) NOT NULL DEFAULT '0',
  `voguepay_merchand_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_to_pay` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_seller_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.sellers: ~3 rows (approximately)
/*!40000 ALTER TABLE `sellers` DISABLE KEYS */;
INSERT INTO `sellers` (`id`, `user_id`, `verification_status`, `verification_info`, `cash_on_delivery_status`, `sslcommerz_status`, `stripe_status`, `paypal_status`, `paypal_client_id`, `paypal_client_secret`, `ssl_store_id`, `ssl_password`, `stripe_key`, `stripe_secret`, `instamojo_status`, `instamojo_api_key`, `instamojo_token`, `razorpay_status`, `razorpay_api_key`, `razorpay_secret`, `paystack_status`, `paystack_public_key`, `paystack_secret_key`, `voguepay_status`, `voguepay_merchand_id`, `admin_to_pay`, `created_at`, `updated_at`) VALUES
	(3, 13, 0, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2019-12-12 22:11:00', '2019-12-24 20:19:27'),
	(6, 16, 1, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 100.00, '2019-12-17 05:50:58', '2020-01-21 08:35:14'),
	(7, 30, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2020-01-21 09:08:02', '2020-01-21 09:08:02');
/*!40000 ALTER TABLE `sellers` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.seller_withdraw_requests
DROP TABLE IF EXISTS `seller_withdraw_requests`;
CREATE TABLE IF NOT EXISTS `seller_withdraw_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `message` longtext,
  `status` int(1) DEFAULT NULL,
  `viewed` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table panyashala_pns_db.seller_withdraw_requests: ~0 rows (approximately)
/*!40000 ALTER TABLE `seller_withdraw_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller_withdraw_requests` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.seo_settings
DROP TABLE IF EXISTS `seo_settings`;
CREATE TABLE IF NOT EXISTS `seo_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisit` int(11) NOT NULL,
  `sitemap_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.seo_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `seo_settings` DISABLE KEYS */;
INSERT INTO `seo_settings` (`id`, `keyword`, `author`, `revisit`, `sitemap_link`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Panyashala', 'PanyaShala', 100, 'https://www.panyashala.com', 'Online Marketplace.', '2020-01-17 03:12:07', '2020-01-16 21:42:07');
/*!40000 ALTER TABLE `seo_settings` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.services
DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sliders` longtext COLLATE utf8_unicode_ci,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `pick_up_point_id` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_shop_user_id` (`user_id`),
  CONSTRAINT `fk_service_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.services: ~1 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `user_id`, `name`, `logo`, `sliders`, `address`, `latitude`, `longitude`, `facebook`, `google`, `instagram`, `twitter`, `youtube`, `slug`, `meta_title`, `meta_description`, `pick_up_point_id`, `created_at`, `updated_at`) VALUES
	(6, 8, 'MODHURIMA CHOWDHURY PROMA', 'uploads/service/logo/NhzoxTH3DA8953cdQDsglnekR5RFudZXtAL2VN03.jpeg', NULL, 'gdfsgdf', 0, 0, NULL, NULL, '', NULL, NULL, 'MODHURIMA-CHOWDHURY-PROMA-', NULL, NULL, NULL, '2020-01-21 17:16:35', '2020-01-21 17:16:35');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.sessions
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table panyashala_pns_db.sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.shops
DROP TABLE IF EXISTS `shops`;
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sliders` longtext COLLATE utf8_unicode_ci,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `pick_up_point_id` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_shop_user_id` (`user_id`),
  CONSTRAINT `fk_shop_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.shops: ~3 rows (approximately)
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` (`id`, `user_id`, `name`, `logo`, `sliders`, `address`, `latitude`, `longitude`, `facebook`, `google`, `instagram`, `twitter`, `youtube`, `slug`, `meta_title`, `meta_description`, `pick_up_point_id`, `created_at`, `updated_at`) VALUES
	(2, 13, 'Mehta Market', 'uploads/products/thumbnail/L3SWrKkybL7DRzUWF5h0CHlDWCa0lWgTGDJTcQo1.png', NULL, '90 Feet Rd, Bhabha Colony, Kankarbagh, Hanuman Nagar, Patna, Bihar 800020, India', 25.591120702260277, 85.15743675217311, NULL, NULL, '', NULL, NULL, 'Kids-Clothes-2', 'My Shop', 'My Shop', NULL, '2019-12-12 22:11:00', '2019-12-23 00:18:55'),
	(4, 16, 'Rajdhani Store', 'uploads/shop/logo/mFjv5ezNFS2pd6ora1NzFGuiWqT23zixseslQzLK.png', NULL, 'Bulla Shahid Road, Lakhibag, Gaya, Bihar 823003, India', 24.7907844738109, 85.01880188816926, NULL, NULL, '', NULL, NULL, 'Rajdhani-Store-4', 'Rajdhani Store', 'Rajdhani Store', NULL, '2019-12-17 05:50:58', '2019-12-23 00:23:36'),
	(5, 30, 'Demo Shop Name', 'uploads/shop/logo/XJxcsOe0EIJzRdKobhM3Ohd6pIGvBeZu6Jk0RKFI.jpeg', NULL, 'Demo Shop Address', 0, 0, NULL, NULL, '', NULL, NULL, 'Demo-Shop-Name-', NULL, NULL, NULL, '2020-01-21 09:08:02', '2020-01-21 09:08:02');
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.sliders
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.sliders: ~2 rows (approximately)
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `photo`, `published`, `created_at`, `updated_at`) VALUES
	(7, 'uploads/sliders/slider-image.jpg', 0, '2019-03-12 11:58:05', '2020-01-15 08:08:52'),
	(9, 'uploads/sliders/plidGzaM6LzMhCIyHDIPIDOTExEQY8Agt94M1OqN.png', 1, '2020-01-15 08:08:34', '2020-01-15 08:08:34');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.staff
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.staff: ~0 rows (approximately)
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.subscribers
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.subscribers: ~2 rows (approximately)
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
INSERT INTO `subscribers` (`id`, `email`, `created_at`, `updated_at`) VALUES
	(1, 'rizvi98ahmad@gmail.com', '2019-12-21 05:09:03', '2019-12-21 05:09:03'),
	(2, 'satyanandsingh457@gmail.com', '2019-12-22 05:28:56', '2019-12-22 05:28:56');
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.sub_categories
DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.sub_categories: ~10 rows (approximately)
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
	(1, 'Dairy Products', 1, 'dairy-products', 'Dairy Products', 'Dairy Products', '2019-03-12 12:13:24', '2019-12-22 23:43:28'),
	(2, 'Rice', 1, 'rice', 'Rice', 'Rice', '2019-03-12 12:13:44', '2019-12-22 23:43:55'),
	(3, 'Pulses', 1, 'pulses', 'Pulses', 'Pulses', '2019-03-12 12:13:59', '2019-12-22 23:44:27'),
	(4, 'Vegetables', 1, 'vegetables', 'Vegetables', 'Vegetables', '2019-03-12 12:18:25', '2019-12-22 23:44:54'),
	(5, 'Books', 2, 'books', 'Books', 'Books', '2019-03-12 12:18:38', '2019-12-22 23:45:19'),
	(6, 'Other Items', 2, 'other-stationery-items', 'Other Stationery Items', 'Other Stationery Items', '2019-03-12 12:18:51', '2019-12-22 23:46:22'),
	(9, 'Sports Items', 4, 'sports-items', 'Sports Items', 'Sports Items', '2019-03-12 12:19:22', '2019-12-22 23:47:49'),
	(10, 'Consumer Electronic', 3, 'Consumer-Electronic-7Wi5V', NULL, NULL, '2019-12-24 14:41:38', '2019-12-24 14:41:38'),
	(11, 'Home Appliance', 3, 'Home-Appliance-q3Zd2', NULL, NULL, '2019-12-24 14:43:26', '2019-12-24 14:44:27'),
	(12, 'Security & Protection', 3, 'Security--Protection-MCa7l', NULL, NULL, '2019-12-24 14:43:57', '2019-12-24 14:43:57');
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.sub_sub_categories
DROP TABLE IF EXISTS `sub_sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `brands` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_sub_category_id` (`sub_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.sub_sub_categories: ~7 rows (approximately)
/*!40000 ALTER TABLE `sub_sub_categories` DISABLE KEYS */;
INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `name`, `brands`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Milk', '["1"]', 'milk', 'Milk', 'Milk', '2019-03-12 12:19:49', '2019-12-22 23:48:44'),
	(2, 1, 'Curd', '["1"]', 'curd', 'Curd', 'Curd', '2019-03-12 12:20:23', '2019-12-22 23:49:39'),
	(3, 4, 'Tomato', '["1"]', 'tomato', 'Tomato', 'Tomato', '2019-03-12 12:20:43', '2019-12-22 23:50:40'),
	(4, 4, 'Potato', '["1"]', 'potato', 'Potato', 'Potato', '2019-03-12 12:21:28', '2019-12-22 23:51:09'),
	(5, 2, 'Basmati Rice', '["1"]', 'basmati-rice', 'Basmati Rice', 'Basmati Rice', '2019-03-12 12:21:40', '2019-12-22 23:51:46'),
	(6, 1, 'Chana Dal', '["1"]', 'chana-dal', 'Chana Dal', 'Chana Dal', '2019-03-12 12:21:56', '2019-12-22 23:52:34'),
	(18, 5, 'Inspirational Books', '["1"]', 'Inspirational-Books', 'Inspirational Books', 'Inspirational Books', '2019-12-23 00:11:43', '2019-12-23 00:11:59');
/*!40000 ALTER TABLE `sub_sub_categories` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.tickets
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `client_viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.tickets: ~0 rows (approximately)
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.ticket_replies
DROP TABLE IF EXISTS `ticket_replies`;
CREATE TABLE IF NOT EXISTS `ticket_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply` longtext COLLATE utf8_unicode_ci NOT NULL,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.ticket_replies: ~0 rows (approximately)
/*!40000 ALTER TABLE `ticket_replies` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_replies` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'customer',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_original` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `country` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.users: ~11 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `provider_id`, `user_type`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `avatar`, `avatar_original`, `address`, `latitude`, `longitude`, `country`, `city`, `postal_code`, `phone`, `balance`, `created_at`, `updated_at`) VALUES
	(8, NULL, 'provider', 'Mr. Customer', 'customer@gmail.com', '2018-12-12 00:00:00', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', '2q2eu2mYUA5pZZmXP60hWNNq48MygEyfzZSkJfu7KlHxxvGjFeNBAAu3NeSU', 'https://lh3.googleusercontent.com/-7OnRtLyua5Q/AAAAAAAAAAI/AAAAAAAADRk/VqWKMl4f8CI/photo.jpg?sz=50', 'uploads/users/TPrFzS3k8KxI35vre60OvazusEjhPNorTBgtjF5P.png', 'Unnamed Road, Palepally, Baddipadaga, Telangana 502375, India', 18.1124372, 79.01929969999992, 'US', 'Demo city', '1234', NULL, 0.00, '2018-10-07 10:42:57', '2020-01-21 17:16:35'),
	(10, NULL, 'admin', 'Admin', 'info@panyashala.com', '2019-12-01 12:42:55', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2019-12-01 12:43:55', '2019-12-22 23:36:10'),
	(11, NULL, 'admin', 'Test Admin', 'admin@mail.com', '2019-12-01 12:42:55', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2019-12-01 12:43:55', '2019-12-22 23:36:10'),
	(13, NULL, 'seller', 'Sunny Singh', 'test@gmail.com', '2019-12-12 22:12:00', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', 'PZJpJAjhEG6HaTqnWg97W9REye6Bw9uMKAkcsLUzbcjSn3Ag1iSguKZLR6og', NULL, NULL, 'NH348, Navi Mumbai, Maharashtra 400702, India', 18.94501290020225, 72.97467890912412, 'IN', 'Mumbai', '2342023', '312312300', 0.00, '2019-12-12 14:31:42', '2019-12-12 22:11:00'),
	(16, NULL, 'seller', 'Test Seller', 'seller@mail.com', '2019-12-17 05:42:58', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', NULL, NULL, 'uploads/p84M6xlMuqGlrgmhTgHM78ikBdLljH4caiRMQI9c.jpeg', NULL, NULL, NULL, 'AF', NULL, NULL, NULL, 0.00, '2019-12-17 05:50:58', '2020-01-21 08:35:14'),
	(20, NULL, 'customer', 'Satyanand Singh', 'satyanandsingh457@gmail.com', '2019-12-22 04:42:28', '$2y$10$39WxQm4.oR.0SXpuRUYAqOUQjih/shjURFbdmAOVSP1b1qra/vgru', 'x3XicoxJrFRdZ8xOTHKXeEPWAltgI8uLKJoloffl0rUPvnG5u3T16Radj2K2', NULL, NULL, 'IGIMS, Sheikhpura, Patna, Bihar 800014, India', 25.6113139, 85.08992820000003, 'IN', 'Patna', '800014', '7320991127', 0.00, '2019-12-22 05:26:28', '2019-12-30 11:45:02'),
	(30, NULL, 'seller', 'Test Customer', 'customer@mail.com', '2020-01-21 12:11:18', '$2y$10$KF5dLHyLrdopZz.4QMCsouE6LD7OKnKTk9.s31JNuLljAtF0GfYRO', 'zGl53R28LFKTx2PfOakZGKiXePBn4MwnH9X367qAb6EaERna6H9sd4hvlOr0', NULL, 'uploads/users/r5fndoqtpqMZKCIiElGarZWPpZMHnU1zvrM0QVN0.jpeg', NULL, 0, 0, 'AF', NULL, NULL, NULL, 0.00, '2019-12-24 20:41:27', '2020-01-21 09:08:02'),
	(31, NULL, 'customer', 'Test', 'technika.valley@mail.com', '2019-12-24 20:42:24', '$2y$10$L0UFRkl94knpMbVHqfbrWeZ4yU0OrrMJYvhV2EaEyHzkZeRgh32c2', NULL, NULL, 'uploads/users/lu1wOJdGZXnfTI4olf2Z0DxbIo1d1X9CyKtZCpbV.png', 'Unnamed Road, Palepally, Baddipadaga, Telangana 502375, India', 18.1124372, 79.01929969999992, 'IN', 'Bangalore', '887788', '7777887788', 0.00, '2019-12-24 20:48:24', '2019-12-24 21:18:21'),
	(32, NULL, 'customer', 'Test3', 'tech.nikavalley@mail.com', NULL, '$2y$10$1BEIUNTqESpELun0i2/Fa.W1JezxMCk6FVVY3MnzVxmee/oitShvK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2019-12-24 20:51:11', '2019-12-24 20:51:11'),
	(33, NULL, 'customer', 'Test', 'test@mail9.xom', '2019-12-24 22:42:46', '$2y$10$dGJLejiKi7VSND7pCecuc.cVNrLBDoNSchSsdVU8wNZxm.JY7Zo7W', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2019-12-24 22:37:46', '2019-12-24 22:37:46'),
	(34, NULL, 'customer', 'Haris ali', 'quickfixesqf@gmail.com', NULL, '$2y$10$VvkqOu16cx.IMLH.qimOEOO4kZx23kq3I1DD0GjKxHu.JkMMkV0tO', 'hx3dxfK7rMY1QhC7KNULUEpght8o6j7KGoICJmTAv7llc0qcr2nZCTsx0sfv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-01-18 08:37:14', '2020-01-18 08:37:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.wallets
DROP TABLE IF EXISTS `wallets`;
CREATE TABLE IF NOT EXISTS `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.wallets: ~0 rows (approximately)
/*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
/*!40000 ALTER TABLE `wallets` ENABLE KEYS */;

-- Dumping structure for table panyashala_pns_db.wishlists
DROP TABLE IF EXISTS `wishlists`;
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table panyashala_pns_db.wishlists: ~0 rows (approximately)
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
