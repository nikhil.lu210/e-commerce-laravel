<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SellerWithdrawRequest;
use Auth;
use App\Seller;
use App\Shop;

class SellerWithdrawRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $latitude= \Session::get('latitude');
            $longitude= \Session::get('longitude');
            $distance = Shop::distance( $latitude,  $longitude);
            $distances = $distance->orderBy('distance', 'ASC')->get();
            $positions= Shop::get();
        $seller_withdraw_requests = SellerWithdrawRequest::where('user_id', Auth::user()->seller->id)->paginate(9);
        return view('frontend.seller.seller_withdraw_requests.index', compact('seller_withdraw_requests', 'latitude', 'longitude', 'distances', 'positions', 'latitude', 'longitude', 'distances', 'positions'));
    }

    public function request_index()
    {
            $latitude= \Session::get('latitude');
            $longitude= \Session::get('longitude');
            $distance = Shop::distance( $latitude,  $longitude);
            $distances = $distance->orderBy('distance', 'ASC')->get();
            $positions= Shop::get();
        $seller_withdraw_requests = SellerWithdrawRequest::all();
        return view('seller_withdraw_requests.index', compact('seller_withdraw_requests', 'latitude', 'longitude', 'distances', 'positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seller_withdraw_request = new SellerWithdrawRequest;
        $seller_withdraw_request->user_id = Auth::user()->seller->id;
        $seller_withdraw_request->amount = $request->amount;
        $seller_withdraw_request->message = $request->message;
        $seller_withdraw_request->status = '0';
        $seller_withdraw_request->viewed = '0';
        if ($seller_withdraw_request->save()) {
            flash(__('Request has been sent successfully'))->success();
            return redirect()->route('withdraw_requests.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payment_modal(Request $request)
    {
        $seller = Seller::findOrFail($request->id);
        $seller_withdraw_request = SellerWithdrawRequest::where('id', $request->seller_withdraw_request_id)->first();
        return view('seller_withdraw_requests.payment_modal', compact('seller', 'seller_withdraw_request'));
    }

    public function message_modal(Request $request)
    {
        $seller_withdraw_request = SellerWithdrawRequest::findOrFail($request->id);
        if (Auth::user()->user_type == 'seller') {
            return view('frontend.partials.withdraw_message_modal', compact('seller_withdraw_request'));
        }
        elseif (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff') {
            return view('seller_withdraw_requests.withdraw_message_modal', compact('seller_withdraw_request'));
        }
    }
}
