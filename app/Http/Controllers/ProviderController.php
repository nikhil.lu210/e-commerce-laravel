<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\User;
use App\Provider;
use App\BusinessSetting;
use Auth;
use Hash;

class ProviderController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('user', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Auth::user()->service;
            $latitude= \Session::get('latitude');
            $longitude= \Session::get('longitude');
            $distance = Service::distance( $latitude,  $longitude);
            $distances = $distance->orderBy('distance', 'ASC')->get();
            $positions= Service::get();
        return view('frontend.provider.service', compact('service', 'latitude', 'longitude', 'distances', 'positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check() && Auth::user()->user_type == 'admin'){
            flash(__('Admin can not be a provider'))->error();
            return back();
        }
        else{
            $latitude= \Session::get('latitude');
            $longitude= \Session::get('longitude');
            $distance = Service::distance( $latitude,  $longitude);
            $distances = $distance->orderBy('distance', 'ASC')->get();
            $positions= Service::get();
            return view('frontend.provider_form', compact('latitude', 'longitude', 'distances', 'positions'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = null;
        if(!Auth::check()){
            if(User::where('email', $request->email)->first() != null){
                flash(__('Email already exists!'))->error();
                return back();
            }
            if($request->password == $request->password_confirmation){
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->user_type = "provider";
                $user->password = Hash::make($request->password);
                $user->save();
            }
            else{
                flash(__('Sorry! Password did not match.'))->error();
                return back();
            }
        }
        else{
            $user = Auth::user();
            if($user->customer != null){
                $user->customer->delete();
            }
            $user->user_type = "provider";
            $user->save();
        }

        if(BusinessSetting::where('type', 'email_verification')->first()->value != 1){
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
        }

        $provider = new Provider;
        $provider->user_id = $user->id;
        $provider->save();

        if(Service::where('user_id', $user->id)->first() == null){
            $service = new Service;
            $service->user_id = $user->id;
            $service->name = $request->name;
            $service->address = $request->address;
            $service->latitude = $request->latitude;
            $service->longitude = $request->longitude;
            $service->slug = preg_replace('/\s+/', '-', $request->name).'-'.$service->id;
            $service->logo = $request->logo->store('uploads/service/logo');

            if($service->save()){
                auth()->login($user, false);
                flash(__('Your Service has been created successfully!'))->success();
                return redirect()->route('services.index');
            }
            else{
                $provider->delete();
                $user->user_type == 'customer';
                $user->save();
            }
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);

        if($request->has('name') && $request->has('address')){
            $service->name = $request->name;
            $service->address = $request->address;
            $service->latitude = $request->latitude;
            $service->longitude = $request->longitude;
            $service->slug = preg_replace('/\s+/', '-', $request->name).'-'.$service->id;

            $service->meta_title = $request->meta_title;
            $service->meta_description = $request->meta_description;

            if($request->hasFile('logo')){
                $service->logo = $request->logo->store('uploads/Service/logo');
            }

            if ($request->has('pick_up_point_id')) {
                $service->pick_up_point_id = json_encode($request->pick_up_point_id);
            }
        }

        elseif($request->has('facebook') || $request->has('google') || $request->has('twitter') || $request->has('youtube') || $request->has('instagram')){
            $service->facebook = $request->facebook;
            $service->google = $request->google;
            $service->twitter = $request->twitter;
            $service->youtube = $request->youtube;
            $service->instagram = $request->instagram;
        }

        else{
            if($request->has('previous_sliders')){
                $sliders = $request->previous_sliders;
            }
            else{
                $sliders = array();
            }

            if($request->hasFile('sliders')){
                foreach ($request->sliders as $key => $slider) {
                    array_push($sliders, $slider->store('uploads/Service/sliders'));
                }
            }

            $service->sliders = json_encode($sliders);
        }

        if($service->save()){
            flash(__('Your Service has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify_form(Request $request)
    {
        if(Auth::user()->provider->verification_info == null){
            $shop = Auth::user()->shop;
            return view('frontend.provider.verify_form', compact('shop'));
        }
        else {
            flash(__('Sorry! You have sent verification request already.'))->error();
            return back();
        }
    }

    public function verify_form_store(Request $request)
    {
        $data = array();
        $i = 0;
        foreach (json_decode(BusinessSetting::where('type', 'verification_form')->first()->value) as $key => $element) {
            $item = array();
            if ($element->type == 'text') {
                $item['type'] = 'text';
                $item['label'] = $element->label;
                $item['value'] = $request['element_'.$i];
            }
            elseif ($element->type == 'select' || $element->type == 'radio') {
                $item['type'] = 'select';
                $item['label'] = $element->label;
                $item['value'] = $request['element_'.$i];
            }
            elseif ($element->type == 'multi_select') {
                $item['type'] = 'multi_select';
                $item['label'] = $element->label;
                $item['value'] = json_encode($request['element_'.$i]);
            }
            elseif ($element->type == 'file') {
                $item['type'] = 'file';
                $item['label'] = $element->label;
                $item['value'] = $request['element_'.$i]->store('uploads/verification_form');
            }
            array_push($data, $item);
            $i++;
        }
        $provider = Auth::user()->provider;
        $provider->verification_info = json_encode($data);
        if($provider->save()){
            flash(__('Your service verification request has been submitted successfully!'))->success();
            return redirect()->route('dashboard');
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }
}
