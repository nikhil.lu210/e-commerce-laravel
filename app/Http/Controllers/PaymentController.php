<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Shop;
use Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latitude= \Session::get('latitude');
        $longitude= \Session::get('longitude');
        $distance = Shop::distance( $latitude,  $longitude);
        $distances = $distance->orderBy('distance', 'ASC')->get();
        $positions= Shop::get();
        $payments = Payment::where('seller_id', Auth::user()->seller->id)->paginate(9);
        return view('frontend.seller.payment_history', compact('payments', 'latitude', 'longitude', 'distances', 'positions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment_histories(Request $request)
    {
        $latitude= \Session::get('latitude');
        $longitude= \Session::get('longitude');
        $distance = Shop::distance( $latitude,  $longitude);
        $distances = $distance->orderBy('distance', 'ASC')->get();
        $positions= Shop::get();
        $payments = Payment::orderBy('created_at', 'desc')->get();
        return view('sellers.payment_histories', compact('payments', 'latitude', 'longitude', 'distances', 'positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $latitude= \Session::get('latitude');
            $longitude= \Session::get('longitude');
            $distance = Shop::distance( $latitude,  $longitude);
            $distances = $distance->orderBy('distance', 'ASC')->get();
            $positions= Shop::get();
        $payments = Payment::where('seller_id', decrypt($id))->orderBy('created_at', 'desc')->get();
        if($payments->count() > 0){
            return view('sellers.payment', compact('payments', 'latitude', 'longitude', 'distances', 'positions'));
        }
        flash('No payment history available for this seller')->warning();
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
