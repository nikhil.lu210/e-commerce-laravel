<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Malhal\Geographical\Geographical;

class Service extends Model
{
  use Geographical;
  protected static $kilometers = true;

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
